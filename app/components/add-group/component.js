import Ember from 'ember';
import Modal from 's/lib/modal';
export default Ember.Component.extend({
	init() {
	this._super(...arguments);	
	this.set("openModal",false);	
	this.set("confirmdelete",false);
}, 
 openmodal(){
	 this.set("openModal",true);	
	 
 },
 remove() {
	 this.get("list").removeAt(this.get("index"));

 },
 add(name) {
	 name = name.trim();
	 var list = this.get("list");
	 if(list.indexOf(name) == -1 && name.length > 0) {
		 var c;
		 var createobj = this.get("createobj");
		 if(createobj) {
			 c = createobj(name);			 
		 }
	 this.get("list").pushObject(c || name);
	 }
	 this.set("groupname","");
 },
 clickremove(index) {
	 this.set("confirmdelete",true);
	 this.set("index",index);
	 return false;
 }

});



