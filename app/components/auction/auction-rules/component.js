import Ember from 'ember';

export default Ember.Component.extend({
	didReceiveAttrs() {
		this._super(...arguments);
		var m = this.get("model");
		if(!m || !m.get) {
		m = Ember.Object.create({});
		this.set("model",m);
		}
		if(!m.get("rules")) {
			Ember.Logger.log("JJJJJJ");
			m.set("rules",[]);
		}
		var r = m.get("rules");
	  this.set("rules",r);
	  if(!r.length) {
		  r.pushObject({});
		//  this.sendAction("create");
	  }
	  this.set("confirmdelete",false);
	},
	remove() {
			this.get("rules").removeObject(this.get("rule"));
	},
	actions :{
	edit(index) {
		this.$('textarea').attr("disabled",true);
		var t = this.$('textarea');
		t.eq(index).removeAttr("disabled");
	},
	clickremove(rule) {
		this.set("rule",rule);
		if(rule.text && rule.text.trim().length) {
		this.set("confirmdelete",true);			
		} else {
			this.remove();
		}
	},
	cancel() {
		//this.set("confirmdelete",false);
	},
	remove() {
		//this.set("confirmdelete",false);
		this.remove();
		//rule.deleteRecord();		
	},
	create() {
	  // var m = this.store.create("rule");
	   this.get("rules").pushObject({});       
	},
	save() {		
		var m = this.get("rules").filter(m=> { return  m.trim().length > 0;});
		var model = this.get("model");
		model.set("rules",m);
		model.save();
	}
	}
});
