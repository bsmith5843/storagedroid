import Ember from 'ember';

export default Ember.Component.extend({
	

	save() {
		//models/auction-listing
		var m = this.get("model");
		m.save();
		for(var im of m.inputs.img) {
			//im = ['Add Picture', 'ImageId1']
			//img = models/image
			var img = m.get(im[1]);
			if(img.changedAttributes().src) {
				img.save();
			}
		}	 
	},
	
	main : [
		["Listing #",'ListingNo',{placeholder: 'Auto Generate ID'}],
		['Auction Start Date','AuctionDate',{type: 'date'}],
		['Start Time','StartTime',{type:'time'}],
		['End Time','EndTime',{type:'time'}],
		['Unit #','UnitNumber',{placeholder:'optional', optional:true}],
		['Unit Size','UnitSize',{placeholder:'optional', optional:true}],
		['Facility Lock Tag ID','FacilityLogTagID',{placeholder:'optional', optional:true}],
		['Additional Info /Unit Contents','AdditionalInfo',{placeholder:'optional', optional:true,type: 'textarea'}],


		],
	img: [
		['Add Picture', 'ImageID1'],
		['Add Picture', 'ImageID2'],
		['Add Picture', 'ImageID3'],
		],
	StorageFacilityName: [
	   ['Associated Storage Facility','StorageFacilityName',{type:'select'}]
	],
	WebSite : [['Web Site', 'WebSite']
	],
	other : [
		['', 'CreatedBy'],
		['','AuctionType']

	]
	
/* 	var auctioneerfields = [
	['Storage Facility Name',  'StorageFacilityName']
	].concat(inputs.main.slice(1,4));

	var facilityfields = [
	 ['Listing Number', 'ListingNo'],
	 ['Facility Name', 'StorageFacilityName'],
	 ["City","City"],
	 ["State","State"],
	 ["Country","Country"],
	 ['Zip Code','Zip']
	 ]; */
	
});
