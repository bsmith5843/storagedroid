import Ember from 'ember';
import Auction from 's/components/auction/create-auction/component';
import Linkto1 from 's/components/link-to1/component';

export default Linkto1.extend({
	
	editlisting(l) {
		window.model.editauction = l;
	    this.linkto1(this.get("editlink"),l.ListingNo || l.FacilityName);
	     
		//this.transitionToRoute(this.get("editlink"), l.ListingNo);
	},
	openModal: false,

	end1(listing) {
		this.set("currentlisting", listing);
		this.set("openModal", true);
	},
	
	end(listing) {
		var a = this.get("AuctionListings");
		a.removeObject(listing);
		//m.save();
	},
	auctionfields : Auction.create().main.slice(1,-2),
	init() {
	  this._super(...arguments);
	//  console.log("KK");
      //console.log(Auction.main);	  
	}
	
	
});
