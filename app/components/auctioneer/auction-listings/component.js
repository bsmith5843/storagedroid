import Ember from 'ember';
import ListAuctions from 's/components/auction/list-auctions/component';
export default ListAuctions.extend({
	auctionfields: [['Facility Name','StorageFacilityName'],
	                ['Auction Date', 'AuctionDate'],
					['Start Time', 'StartTime'],
					['End Time','EndTime']
					],
	editlink : 'user.auctioneer.auctions.edit'
					
});
