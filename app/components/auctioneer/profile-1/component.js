import Ember from 'ember';
import Lib from 's/lib/lib';

export default Ember.Component.extend({
	init() {
		this._super(...arguments);
		var m = this.get("model");
		if(!m) {return;}
		
		var l = Ember.get(m,"Licenses");
		if(!this.get("edit")) {
			Ember.set(m,"Licenses",l.filter((v)=>{ return v.LicenseId;}));			
		}
	},
	component : Ember.computed("edit",function(e) { 
      var c = "metro-ui/input";
	  if(!this.get("edit")) {
		  c = "input-field";
		  var i1 = [["Country", "Country",{type : 'select'}],
                	["State", "State",{type : 'select'}]];
		  var i = this.get("inputs2");
		  i.splice(1,1, ...i1);
		  console.log(i);
		  console.log("AA");
		  this.set("inputs2", i)
		 
	  }
	  return c;
	  }),
     tagName: '',
	//profile : this.get("model").get("profile")
	inputs1 : 
	[	
	["First Name", "FirstName"],
	["Last Name", "LastName"],
	["Company Name", "CompanyName"],
	["Street Address", "Address"],	
	["Street Address2", "Address2"],	
//	["Select Country", "Country",{type : 'select'}],
//	["Select State", "State",{type : 'select'}],
   ],
//
inputs2 : [
  ['About my Company','AboutMyCompany',{type: 'textarea', layout: "col-xs-12 marginbottom10"}],   
['Country/State','Country/State', {component: 'country-region-select', id:"country", regionid :"region"}],
	["City", "City"],
	["Postal Code", "Zip"],    
			
	 ["Phone #", "PhoneNo"],	 
	  ["Fax", "Fax"]],
inputs3 : [
	["Enter Email/Login ID", "UserName"],
	["Enter Password", "Password"],	
    ["Confirm Password", ""]   
    ],
	auctioneertype : 
	[	     
	[ "I am a self Storage district manager listing auctions", "SelfStorageDistrictManager"],
	[ "I am an Auctioneer listing auctions", "AuctioneerListingAuction"],
	[ "I am an Auctioneer listing online auctions", "AuctioneerListingOnlineAuction"]
	],
	auctioneertype1 : 
	[	     
	[ "Self Storage district manager listing auctions", "SelfStorageDistrictManager"],
	[ "Auctioneer listing auctions", "AuctioneerListingAuction"],
	[ "Auctioneer listing online auctions", "AuctioneerListingOnlineAuction"]
	],
	l: [ "# Licenses", "Licenses", {type : 'number'}],
   licensesinput :
	[
	
	[ "LicenseID", "LicenseId"],
	[ "License State", "LicenseState"]
	],
	changelicense() {
		var m = this.get("model");
		var l = Ember.get(m,"Licenses");
		var num = Ember.get(m,"TotalLicenses");
		
		var l1 = Lib.addunits(l,num, {"LicenseId" : "", "LicenseState": ""}, "LicenseId");
		console.log(l);
		
		
		console.log(l1);
		
		Ember.set(m,"Licenses",l1);
	}
	
	

});
