import Ember from 'ember';
import ListAuctions from 's/components/auction/list-auctions/component';
import Auction from 's/components/auction/create-auction/component';

export default ListAuctions.extend({
	tagName: '',
	auctionfields : Auction.create().main.slice(1,-2)
	
});
