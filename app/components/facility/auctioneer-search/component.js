import Ember from 'ember';
import Linkto1 from 's/components/link-to1/component';
export default Linkto1.extend({
	tagName : '',
	
	address : [
		['Country','Country'],
		['State','State'],
		["City", "City"],
		["Postal Code", "Zip"], 
	
	]
});
