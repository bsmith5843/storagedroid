import Ember from 'ember';

export default Ember.Component.extend({
	init() {
		this._super(...arguments);
		this.set("option", this.get("unitpage") ? "ClimateControl" : "CoveredParking");
		this.set("checked1","true");
	}
});
