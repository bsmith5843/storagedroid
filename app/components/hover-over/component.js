import Ember from 'ember';

export default Ember.Component.extend({
	tagName: '',
	didInsertElement() {
		this._super(...arguments);
		setTimeout(()=>{ 
		 // $('[data-toggle="popover"]').clickover({global_close:true});
		  $('[data-toggle="popover"]').popover({delay: {show: 0, hide: 500}});
		},50);
	}
});
