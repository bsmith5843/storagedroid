import Ember from 'ember';

export default Ember.Component.extend({
	tagName: "inline",
   click() { 
     //opens select file popup on click of image
    
   },	   
   //imgsrc : this.get("id") ? "/images.php?id=" +this.get('id') : "/images/Add_Picture.png",   
  
    didInsertElement() {
		this._super(...arguments);
		var img = this.$('img');
		//scales image
		
		img.imageScale();	
		this.$('div').on('click', () => {  this.$('input').click();});
		var model = this.get("model");
		if(!model) {this.set("model",Ember.Object.create());}
		model = this.get("model");
        //displays image after upload		
		this.$('input').on('change',function (e) {							
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {  				  
				  // img.attr("src",e.target.result);
				  
				   model.set("src", e.target.result);
                    //rescale image	, set timeout to avoid bug
                 Ember.Logger.log("img log");		
                setTimeout(()=>{				 
				 img.imageScale("destroy");	
				  img.imageScale("scale");		
				},10);
				 //  img.imageScale();		
				};
                reader.readAsDataURL(this.files[0]);				
            }
	     } );
	 }
	 
	

		/* $.ajax( {
			url : `/images.php?name=${this.get('name')}`,
			
			 
		 });
		  /* window.loadImage( e.target.files[0], function (im) {
              img.attr('src', im.src);
			  img.imageScale();		
          });
			*/
		
	 


});

