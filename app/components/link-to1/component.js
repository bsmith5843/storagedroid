import Ember from 'ember';

export default Ember.Component.extend({
	tagName : '',
	routing: Ember.inject.service('-routing'),
	
	linkto1(r,id) {		
	    var a = id ? [r, [id], [], false] : [r];
		this.get("routing").transitionTo(...a);		
	}
});
