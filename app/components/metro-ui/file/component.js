import Ember from 'ember';
 
export default Ember.Component.extend({
	tagName: '',
	didInsertElement() {
		this._super(...arguments);  
		var p = this.get("placeholder");
		
		if(p) {			
			setTimeout(()=>{
		 $(".input-file-wrapper").attr({placeholder: p});
			},50);
		}
	}
});
