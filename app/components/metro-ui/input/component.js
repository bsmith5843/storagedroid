import Ember from 'ember';

export default Ember.Component.extend({
	tagName:'',	
	init() {
		this._super(...arguments);
		this.set("html5time", Modernizr.inputtypes.time);
		this.set("html5date", Modernizr.inputtypes.date);
	},
	didInsertElement() {
		var v = this.get("validate");
		if(v) {
			$(`[name="${this.get('name')}"`).attr("data-validate-func",v);
		}

	}
	
});
