
import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'input',
  type: 'checkbox',
  attributeBindings: ['type', 'htmlChecked:checked','value', 'name', 'disabled'],
/*
  value: null,
  checked: null,
 */
  htmlChecked: Ember.computed('value', 'checked', function() {
	
    return this.get('value') === this.get('checked');
  }),
  click() {
	 
    this.set('checked', this.get('value'));
    //this._setCheckedProp();
	this._setCheckedProp();
	return true;
  },
 

  _setCheckedProp: function() {
	 
    if (!this.$()) { return; }	
    this.$().prop('checked', this.get('value') === this.get('checked'));
	//this.$().closest("label, body").off("change").off("click");
  },

  _updateElementValue: Ember.observer('htmlChecked', function() {
    Ember.run.once(this, '_setCheckedProp');
  })
});
