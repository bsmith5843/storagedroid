import Ember from 'ember';

export default Ember.Component.extend({
	tagName : '',

	
	didInsertElement() {
		var id = "#" + this.get("id");
		var v = this.get("value");
		
		//AM PM
		if(v && v.match("M")) {
			//v= 14:40 // moment("02:40 PM", "HH:mm A").format("HH:mm");			
		  	v = moment(v, "HH:mm A").format("HH:mm");
		}
			
		
		var init = v ? "0000-00-00 " + v +":00" : '';
		//var init = '';
		
		$(id).timeinput({init});
		
		$(id).timeinput("show");
		if(this.get("value")) {
		
			//$(id).
		}
		//$(document).on("keyup"
		
		$(id).on("keyup1",this.change1.bind(this));
	},
	change1() {
		var t = $("#" + this.get("id")).text() || "";
		var v = t && moment(t, "HH:mm A").format("HH:mm"); 
		
		this.set("value", v);
		console.log("value change " + v);
	}
	
	/*
	
	 	var mm = moment("02:40 PM", "HH:mm A").format("HH:mm");
	console.log(mm);
     $( "div.f" ).timeinput( {start:1, init: "0000-00-00 " + mm + ":00"} );
	$( "div.f" ).filthypillow( {start:1} );
      $( "div.f" ).filthypillow( "show" );
	  $( "div.f" ).append(" - ");
	   $( "div.f" ).filthypillow( {end:1} );
      $( "div.f" ).filthypillow( "show" );

	*/
	
});
