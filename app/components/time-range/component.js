import Ember from 'ember';

export default Ember.Component.extend({
	tagName : '',
	
	show() {
		var id = "#" + this.get("id");
		$(id).timeinput({start: 1});
		$(id).timeinput("show");
		$(id).append("-");
		$(id).timeinput({end: 1});
		$(id).timeinput("show");
	},
	destroy() {
		var id = "#" + this.get("id");
		$(id).find(".fp-option").text("--");
		
	},
	didInsertElement() {
		this.show();
		//$(`[name="${this.get("id")}"]`).appendTo($(id));
		
	}
});
