import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service(),

  isLoading: false,

  actions: {
    authenticate: function() {
      let { identification, password } = this.getProperties('identification', 'password');
      let attemptLogin = this.get('session').authenticate('authenticator:oauth2', identification, password);
      
      this.set('isLoading', true);

      attemptLogin().then(() => {
        this.set('isLoading', false);
      }).catch(reason => {
        this.set('errorMessage', reason.error || reason);
      });
    }
  }

});
