import Ember from 'ember';

export function any(params/*, hash*/) {
  return params.find(v=>v) || "";
}

export default Ember.Helper.helper(any);
