import Ember from 'ember';

export function back(params/*, hash*/) {
  return function() { history.back();};
}

export default Ember.Helper.helper(back);
