import Ember from 'ember';

export function combinefunc(params/*, hash*/) {
  return function() { params[0](); params[1]();};
}

export default Ember.Helper.helper(combinefunc);
