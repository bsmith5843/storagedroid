import Ember from 'ember';

export function concat1(params/*, hash*/) {
  return params[0].concat(params[1]);
}

export default Ember.Helper.helper(concat1);
