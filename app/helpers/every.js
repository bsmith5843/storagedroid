import Ember from 'ember';

export function every(params/*, hash*/) {
  return params.every(v=>v);
}

export default Ember.Helper.helper(every);
