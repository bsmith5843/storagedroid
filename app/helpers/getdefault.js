import Ember from 'ember';

export function getdefault(params/*, hash*/) {
  return params[0] ? ( params[0][params[1]] || "" ) : "";
}

export default Ember.Helper.helper(getdefault);
