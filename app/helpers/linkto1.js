import Ember from 'ember';

var	routing = Ember.inject.service('-routing');

export function linkto1(params/*, hash*/) {
  return function() { 
        var r = params[0], id = params[1];  
	    var a = id ? [r, [id], [], false] : [r];
		routing.transitionTo(...a);		
	}; 
}

export default Ember.Helper.helper(linkto1);
