import Ember from 'ember';

export function radiocheckbox(params/*, hash*/) {
	
	var checked = !!params[0] && !!!params[0].toString().match(/false|no/i);
	 return checked;
}

export default Ember.Helper.helper(radiocheckbox);
