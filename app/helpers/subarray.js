import Ember from 'ember';

export function subarray(params/*, hash*/) {
	params[0] = params[0] || [];
  return params[0].slice(params[1],params[2]);
}

export default Ember.Helper.helper(subarray);
