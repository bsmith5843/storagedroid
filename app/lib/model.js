import DS from 'ember-data';

export default function(inputs, types) {   
     var model = {inputs };
	 Object.keys(inputs).forEach(v=> {
     	inputs[v].forEach(v1 => {
		  
		  model[v1[1]] = types[v] ? DS.attr(types[v]) : DS.attr();
		  v1[2] || (v1[2] = {});
		  v1[2].validate = v1[2].optional ? "" : v1[2].required ? "required" : (v1[2].validate || "");
	   });
   });
     return model;
 }