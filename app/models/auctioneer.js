import DS from 'ember-data';
import M from 's/lib/model';
import Profile from './profile';
var inputs = {
  //contact:  Profile.inputs.address.concat([ "Enter Fax", "fax"]),

//	login: Profile.login.concat([ "About my Company", "AboutMyCompany"]),
	//CompanyName  
	auctioneergroup1 : 
	[	     
	[ "I am a self Storage district manager listing auctions", "SelfStorageDistrictManager"],
	[ "I am an Auctioneer listing auctions", "AuctioneerListingAuction"],
	[ "I am an Auctioneer listing online auctions", "AuctioneerListingOnlineAuction"]
	],
   auctioneergroup2 :
	[
	[ "# Licenses", "Licenses"],
	[ "LicenseID", "LicenseId"],
	[ "License State", "LicenseState"]
	],
	
    auctioneerother : 
	[
	['Company Name','CompanyName'],
     ['','AboutMyCompany']
	]
 
};
var m = M(inputs, {});
m.rules = DS.attr();
m.user = DS.belongsTo('user');
m.profile = DS.belongsTo('profile');
m.searchresultfields = [
 ['', 'Country'], 
 ['','State'],
 ['','City'],
 ['Zip Code','Zip']
 ];
//  options :  DS.attr(),
 // emailpref : DS.attr(),


export default DS.Model.extend(m);
