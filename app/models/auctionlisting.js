import DS from 'ember-data';
import M from 's/lib/model';


var inputs = { 
main : [
["Listing #",'ListingNo',{placeholder: 'Auto Generate ID'}],
['Auction Start Date','AuctionDate',{type: 'date'}],
['Start Time','StartTime',{type:'time'}],
['End Time','EndTime',{type:'time'}],
['Unit #','UnitNumber',{placeholder:'optional', optional:true}],
['Unit Size','UnitSize',{placeholder:'optional', optional:true}],
['Facility Lock Tag ID','FacilityLogTagID',{placeholder:'optional', optional:true}],
['Additional Info /Unit Contents','AdditionalInfo',{placeholder:'optional', optional:true,type: 'textarea'}],


],
img: [
['Add Picture', 'ImageID1'],
['Add Picture', 'ImageID2'],
['Add Picture', 'ImageID3'],
],
StorageFacilityName: [
['Associated Storage Facility','StorageFacilityName',{type:'select'}]
],
WebSite : [['Web Site', 'WebSite']
],
other : [
['', 'CreatedBy'],
['','AuctionType']

]
};
var auctioneerfields = [
['Storage Facility Name',  'StorageFacilityName']
].concat(inputs.main.slice(1,4));

var facilityfields = [
 ['Listing Number', 'ListingNo'],
 ['Facility Name', 'StorageFacilityName'],
 ["City","City"],
 ["State","State"],
 ["Country","Country"],
 ['Zip Code','Zip']
 ];
 var facilityfields1 = inputs.main.slice(0,7);


 var m = M(inputs,{}); 
 //'img':'number'
 
 m.ImageID1 = DS.belongsTo("image");
 m.ImageID2 = DS.belongsTo("image");
 m.ImageID3 = DS.belongsTo("image");
 
m.auctioneerfields = auctioneerfields;
m.facilityfields = facilityfields;
m.facilityfields1 = facilityfields1;
m.auctionType = DS.attr();
m.facility = DS.belongsTo('facility');
m.facilityprofile = DS.belongsTo("profile");
m.CreatedBy = DS.belongsTo('user');
m.online = DS.attr();
m.onlinechange = Ember.observer("auctionType","AuctionType",function() {
	var online = this.get("auctionType") == "online";
	this.set("online",online);
});

export default DS.Model.extend(m);
