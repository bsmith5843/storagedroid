import DS from 'ember-data';

export default DS.Model.extend({
  AuctioneerSetRules :DS.attr(),
  auctioneerEmail : DS.attr(),
  rules : DS.attr(),
  user : DS.belongsTo('user'),
  facility : DS.belongsTo('facility'),
  auctioneer : DS.belongsTo('auctioneer')
  
});
