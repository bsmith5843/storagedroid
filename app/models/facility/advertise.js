import DS from 'ember-data';
import M from 's/lib/model';



var inputs = {
	card: [
['Name on the card', 'NameonCard'],
['Type', 'Type'],
['Credit Card #', 'CardNumber'],
['Expiry Date', 'expiryDate'],
['CSC','CSC']

],

billing : [
['Street Address 1', 'StreetAddress1'],
['Street Address 2', 'StreetAddress2',{optional: true}],
['Apt/Suite #', 'AptSuit',{optional: true}],
//['Country','Country', {type:'select', class:'country'}],
//['State','State',{type:'select'}],
['Country/State','Country/State', {component: 'country-region-select', id:"billingcountry", regionid :"billingregion"}],
['City','City'],
['Postal Code','PostalCode'],
['Phone #','Phone']
],

other : [
['Type of Advertisement','TypeofAdvertisement'],
['Duration','Duration'],
['','EmailID'],
['','CityID']
]

};
var m = M(inputs, {});
	
m.facility = DS.belongsTo("facility");
export default DS.Model.extend(m);

