import DS from 'ember-data';
import M from 's/lib/model';

           
var inputs = {
	 options :[
	 [ 'SSA (Self Storage Association) member', 'SSA'],
	['National Portable Self Storage Association', 'SSANational'],
	['Mobile Self Storage Association', 'SSAMobile'],
	['Canadian Self Storage Association', 'SSACanada'],
	['Self Storage Association of Australia', 'SSAAustralia'],
	['Federation of European Self Storage Association', 'SSAEurope'],
	['UK Self Storage Association', 'SSAUK'],
	['RV/Boat/Vehicle rental space available', 'RvBoatVehicleRentalSpace'],
	['Moving truck available for free', 'MovingTruckAvailableForFree'],
	['Moving truck available for rent', 'MovingTruckAvailableForRent'],
	['Can Store Wine', 'CanStoreWine'],
	['Drive up access', 'DriveUpAccess'],
	['Boxes and supplies for purchase', 'BoxesAndSuppliesForPurchase'],
	['Climate Control', 'ClimateControl'],
	['Online Bill Pay', 'BillPay'],
	['Business Center Available', 'BusinessCenterAvailable'],
	["Storage Renter's insurance available", 'StorageRenterInsurance'],
	['24 Hr security cameras', '_24HourSecurityCameras'],
	['24 Hr Access(Electronic gate with gate code access)', '_24HourAccessElectronicGate']
	], 
	hours : [
	  ["Monday-Friday", 'MonFri'],
	  ['Saturday','Sat'],
	  ['Sunday','Sun']
	],
	cardtypes :[
	['Visa','Visa'],
	["Mastercard",'Mastercard'],
	["Amex" , 'Amex'],
	["Discover",'Discover']
	],
	other : [
	['Move in specials', 'MoveInSpecials', {placeholder : "$1 first month"}],
	['Associated webpage address', 'AssociatedWebpageAddress']
	],
	about: [
	['General Directions to the Facility','GeneralDirection'],
	['Neighborhoods', 'neighborhoods'],
	['About Facility Manager', 'AboutManager'],
	['About Facility Staff', 'AboutStaff'],
	['Other Sales (grills, ice, propane, etc.)','OtherSales'],
	['Associated Rental Trucks/Moving Company (UHaul, Budget, Ryder, Penske)', 'AssociatedRentals']
],
     
 other2: [
   ['Years in Business', 'YearsinBusiness'],
   ['Add Storage Facility Picture', 'FacilityPicture']
   ]
	
	
	
};
var types = { hours : "string", options : "truefalsestring", other:"", cardtypes : "" };
var m = M(inputs, types);
m.selectedoptions = m.inputs.options.filter((v) => {
			return m[v[1]]==true || m[v[1]] == "true";
});	
export default DS.Model.extend(m); 

