import DS from 'ember-data';

export default DS.Model.extend({
  Size : DS.attr(),  
  Option : DS.attr("truefalsestring"),
  Quantity : DS.attr(),  
  Price : DS.attr(),
  facility : DS.belongsTo("facility")  

});
