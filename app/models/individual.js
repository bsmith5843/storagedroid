import DS from 'ember-data';
/*
var fields = ["Enter First Name", "firstname"],
	["Enter Last Name", "lastname"],
	
	["Enter Street Address", "address"],
	["About my Company", "description"],
	["Select Country", "country"],
	["Select State", "state"],
	["Enter City", "city"],
	["Enter Postal Code", "post"],
	["Enter Phone #", "phone"],	
	[ "Enter Email/Login ID", "login"],
	[ "Enter Password", "password"],
	["Enter Confirm Password", ""]];
	*/
export default DS.Model.extend({
  user : DS.belongsTo('user'),
  profile : DS.belongsTo("profile"),
  favoriteauctions : DS.hasMany('auctionlisting'),
  emailpref : DS.attr()
});
