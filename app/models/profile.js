import DS from 'ember-data';
import M from 's/lib/model';
var inputs =  {
	   address : [	
	["Enter Street Address", "Address"],	
	["Enter Street Address2", "Address2"],	
//	["Select Country", "Country",{type : 'select'}],
//	["Select State", "State",{type : 'select'}],
['Country/State','Country/State', {component: 'country-region-select', id:"country", regionid :"region"}],
	["Enter City", "City"],
	["Enter Postal Code", "Zip"]
	],
	contact :[	
	["Enter First Name", "FirstName"],
	["Enter Last Name", "LastName"],		
	 ["Enter Phone #", "PhoneNo"]
	 ],
	 login : [
	["Enter Email/Login ID", "UserName"],
	["Enter Password", "Password"],	
    ["Enter Confirm Password", ""]   
    ],
	facilityoptional : [
	 ["Facility Owner", "FacilityOwner"],	
	 ["Facility Owner Email", "FacilityOwnerEmail"],	
	 ["Facility Manager", "FacilityManager"],	
	 ["Facility Manager Email", "FacilityManagerEmail"],		 
	["Enter Fax", "fax"],
   ],        
   facilityother : [
   ['', 'FacilityName'],
   ['','StoreNo'],
   ['','img']   
   ] ,
   auctioneergroup1 : 
	[	     
	[ "I am a self Storage district manager listing auctions", "SelfStorageDistrictManager"],
	[ "I am an Auctioneer listing auctions", "AuctioneerListingAuction"],
	[ "I am an Auctioneer listing online auctions", "AuctioneerListingOnlineAuction"]
	],
   auctioneergroup2 :
	[
	[ "# Licenses", "Licenses"],
	[ "LicenseID", "licenseid"],
	[ "License State", "licensestate"]
	],
	
    auctioneerother : 
	[
	['','CompanyName'],
     ['','AboutMyCompany']
	]
};
var m = M(inputs,{});
m.user = DS.belongsTo("user");
m.img = DS.belongsTo("image");
export default DS.Model.extend(m);



