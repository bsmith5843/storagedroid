import DS from 'ember-data';

//FacilityName  StoreNo  Address  Address2  City  State  Zip  Country  Email  UserName  Password  UserType  LastName  FirstName  CreatedDate  Longitude  Latitude  PhoneNo  Fax  FacilityOwner  FacilityOwnerEmail  FacilityManager  FacilityManagerEmail  CompanyName  AboutMyCompany  SelfStorageDistrictManager  AuctioneerListingAuction  AuctioneerListingOnlineAuction  Licenses  

export default DS.Model.extend({
  "ID" : DS.attr(),
   "Password" :DS.attr(),
   'UserType' : DS.attr(),
   "profile" : DS.belongsTo("profile"),
   'individual' : DS.belongsTo("individual"),
   'facility' : DS.belongsTo("facility"),
   'auctioneer' : DS.belongsTo("auctioneer")
   
});
