import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('user', {path: '/'} ,function() {
    this.route('facility',  function() {
      this.route('profile', function() {
        this.route('options');  
        this.route('options2');
      });
     
      this.route('create');
      this.route('view');
      this.route('advertise', function() {
        this.route('payment');
        this.route('units');
      });
      this.route('payment');
      this.route('find-auctioneer',function() {
          this.route("result", {path: '/:id'});
      });
      this.route('units');
      this.route('rules', function() {});
      this.route('auctions', function() {
        this.route('createauction');
        this.route('edit', {path: '/edit/:id'});
      });
      
      this.route('createauction');
    });

    this.route('auctioneer', function() {
      this.route('edit');
      this.route('rules');
      this.route('groups', function() {
          this.route('group',{path:'/:id'});
      });
      this.route('auctionupload');
      this.route('auctions', function() {
        this.route('createauction');
        this.route('edit', {path: '/edit/:id'});
      });
      this.route('profile');
    });

    this.route('user', function() {
      this.route('searchlistings');
      this.route('emailpref');
      this.route('profile');
    });
  });

  this.route('search');
  this.route('email');
  this.route('login');
  this.route('createauction');
});

export default Router;
