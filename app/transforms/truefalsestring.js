import DS from 'ember-data';

export default DS.Transform.extend({
  deserialize(serialized) {
    return serialized ? "true" : "false";
  },

  serialize(deserialized) {
    return deserialized == "true" ? true : false;
  }
});
