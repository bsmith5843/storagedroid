import Ember from 'ember';

export default Ember.Controller.extend({
	init() {
		this._super(...arguments);
		$(window).on("resize",()=>{
			var w = $("body").width();
			this.togglenav(w);
			
		});
		$('body').on("click touch", ()=>{
			if($("body").width()<500 && $('.navbar').width() > 0){
			this.togglenav("toggle");
			}
		});
		
	},
	
	togglenav(t) {
		Ember.Logger.log("toggle");
		var n = $('.navbar');
		var w = n.width();
		var p = n.position().left;
		
		if(t=="toggle") {
			if(p < 0 || w ==0) {
				n.css({position: "absolute"});
				n.animate({left: "0px", width: "200px"},500);
			}
			else {
				n.animate({left: "-500px", width: "0px"},500);
			}
			$('.navbar1').css({width:"0px"});
			
		}
		else {
			if ( t< 500 && (!n.attr("m"))) {
				Ember.Logger.log("t < 500");
				var css = {position: "absolute", left: "-500px", width: "0px"};
				n.css(css);
				$('.navbar1').css({width:"0px"});
				//n.css(css);
			}
			else if (t > 500) {
				Ember.Logger.log("t > 500");
				var css = {position: "relative", left: "0px", width: "200px"};
				n.css(css);
				$('.navbar1').css({width:"200px"});
				//n.css(css);
				
			}
		}
       		
		
	},
	
	menuclick() {
		this.togglenav("toggle");
	}
});
