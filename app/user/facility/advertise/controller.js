import Ember from 'ember';

export default Ember.Controller.extend({
	next() {
		var m = this.get("model");
		m.save();
        this.transitionToRoute("facility/payment" ,m);
	}
});
