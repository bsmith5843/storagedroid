import Ember from 'ember';

export default Ember.Controller.extend({
	/*init() {
		setTimeout(()=>{console.log(window.model);},1000);
	}*/
	next() {
		var m = this.get("model");
		m.save();
        this.transitionToRoute("facility/payment" ,m);
	}
});
