import Ember from 'ember';

export default Ember.Controller.extend({
	next() {
		var m = this.get("model");
		m.save();
		this.transitionToRoute("facility.units", m.facility);
	},
	card: [
	['Name on the card', 'NameonCard'],
	['Type', 'Type'],
	['Credit Card #', 'CardNumber'],
	['Expiry Date', 'expiryDate'],
	['CSC','CSC']

	],

	billing : [
	['Street Address 1', 'StreetAddress1'],
	['Street Address 2', 'StreetAddress2',{optional: true}],
	['Apt/Suite #', 'AptSuit',{optional: true}],
	//['Country','Country', {type:'select', class:'country'}],
	//['State','State',{type:'select'}],
	['Country/State','Country/State', {component: 'country-region-select', id:"billingcountry", regionid :"billingregion"}],
	['City','City'],
	['Postal Code','PostalCode'],
	['Phone #','Phone']
	],

	other : [
	['Type of Advertisement','TypeofAdvertisement'],
	['Duration','Duration'],
	['','EmailID'],
	['','CityID']
	]
});
