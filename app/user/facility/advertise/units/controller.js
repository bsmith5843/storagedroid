import Ember from 'ember';
import Lib from 's/lib/lib';
export default Ember.Controller.extend({	
   
	modeltemp : Ember.computed("unitpage", function() {
		if(this.get("unitpage")) {
			 return this.get("model").get("units");
		}
			 return this.get("model").get("parking");
	}),
	
	uniqesizes : Ember.computed("modeltemp.FacilityUnits.@each.Size", function() {
		var unitsizes = [];
		this.get("modeltemp").get("FacilityUnits").forEach(v => {
			 var s = v.get("Size").trim();
			if(unitsizes.indexOf(s) == -1) {unitsizes.push(s);}
		});
		return unitsizes;
	}),
	
	submit() {
		var model = this.get("modeltemp");
		var summedunits = this.sumunits(model.get("FacilityUnits"));
		var sumavailable = this.sumunits(model.get("AvailableUnits"));
		var totalsizes = this.getint(model.get('TotalDiffUnitSizes'));
		var totalunits = this.getint(model.get("TotalUnits"));
		var errors = [];
		if(summedunits != totalunits) {
			errors.pushObject("summed units is not equal to total units");
		}
        if(sumavailable > summedunits) {
			errors.pushObject("available units is larger than total units");
		}		
		if(totalsizes != this.get("uniquesizes").length) {
			errors.pushObject("total sizes are incorrect");
		}
		errors.forEach(v=> {
			
		});
		if(!errors.length) {
			Ember.Logger.log("submit no errors");
			model.save();			
		} else {
			Ember.Logger.log(errors);
		}
		
	},
	
	
	sumunits(a) {
		return a.reduce((prev, cur)=> {
			return prev + (parseInt(cur.Quantity,10) ||0);
		},0 );
	},	
	changesize() {
		var c = this.get("unitpage") ? "ClimateControl" : "CoveredParking";
		var o = { "Size" : "",    "Quantity" : "",     "Price" : ""    };
		o[c] = "";
		var m = this.get("modeltemp");
		
		
		var unitsizes = Ember.get(m,"FacilityUnits");
		var num = Ember.get(m,"TotalDiffUnitSizes");
		var unitsizes1 = Lib.addunits(unitsizes, num, o,"Size", 50);
		Ember.set(m,"FacilityUnits",unitsizes1);
		
		var availableunits = Ember.get(m,"AvailableUnits");	
		var availableunits1 = Lib.addunits(availableunits, num, o,"Size", 50);
		Ember.set(m,"AvailableUnits",availableunits1);
	
		Ember.Logger.log(num);
		 
		
		
		
	},
	actions : {
		
		units() {
			
			//this.set("modeltemp", this.get("model").get("units"));
			this.set("unitpage", true);
		},
		parking() {
			//this.set("modeltemp", this.get("model").get("parking"));
			Ember.Logger.log("parking");
			this.set("unitpage", false);			
		}
	},
	
	unitpage : true,
	getint(a){ 
	   return parseInt(a,10) || 0;
	}
	
});
