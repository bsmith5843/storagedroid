import Ember from 'ember';

export default Ember.Controller.extend({
	
	createauction(online){
		var type = online ? "online" : "live";
		var m = this.get("model");
		var r = this.store.createRecord('auctionlisting',
		         {online, auctionType: type, facility: m, CreatedBy : m.user});
		this.transitionToRoute('user.facility.createauction');
	},
	end(listing) {
		var m = this.get("model");
		m.auctionlistings.removeObject(listing);
		m.save();
	}
	
});
