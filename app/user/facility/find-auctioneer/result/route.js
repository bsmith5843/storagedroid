import Ember from 'ember';

export default Ember.Route.extend({
	model(param) {
		var id = param.id;
		return window.searchresults && window.searchresults.findBy("_id",id);
		
	}
});
