import Ember from 'ember';

export default Ember.Controller.extend({
	//profile : this.get("model").get("profile")
	inputs : {
	   address : [	["Enter Street Address", "Address"],	
	["Street Address2", "Address2"],	
//	["Select Country", "Country",{type : 'select'}],
//	["Select State", "State",{type : 'select'}],
['Country/State','Country/State', {component: 'country-region-select', id:"country", regionid :"region"}],
	["Enter City", "City"],
	["Enter Postal Code", "Zip"]
	],
    contact :[	
	["Enter First Name", "FirstName"],
	["Enter Last Name", "LastName"],		
	 ["Enter Phone #", "PhoneNo"]
	 ],
	 login : [
	["Enter Email/Login ID", "UserName"],
	["Enter Password", "Password"],	
    ["Enter Confirm Password", ""]   
    ],
	facilityoptional : [
	 ["Facility Owner", "FacilityOwner"],	
	 ["Facility Owner Email", "FacilityOwnerEmail"],	
	 ["Facility Manager", "FacilityManager"],	
	 ["Facility Manager Email", "FacilityManagerEmail"],		 
	["Enter Fax", "fax"],
   ],  	
	}
});
