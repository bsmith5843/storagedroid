import Ember from 'ember';

export default Ember.Route.extend({
	model() {
		return Ember.Object.create( { "options":       {"SSA":0,"SSANational":0,"SSAMobile":0, 
            "SSACanada":0,"SSAAustralia":0,"SSAEurope":0,"SSAUK":0,"RvBoatVehicleRentalSpace":0,"MovingTruckAvailableForFree":0,"MovingTruckAvailableForRent":0,"CanStoreWine":0,"DriveUpAccess":0,"BoxesAndSuppliesForPurchase":0,"ClimateControl":0,"BillPay":0,"BusinessCenterAvailable":0,"StorageRenterInsurance":0,"_24HourSecurityCameras":0,"_24HourAccessElectronicGate":0},
        "hours":  {"MonFri":"","Sat":"","Sun":""},
        "cardtypes":   {"Visa":0,"Mastercard":0,"Amex":0,"Discover":0},           
        "other":{"MoveInSpecials":"","AssociatedWebpageAddress":""},
        "about": {"GeneralDirection":"","neighborhoods":"",
            "AboutManager":"","AboutStaff":"","OtherSales":"","AssociatedRentals":""}
		//return this.peekRecord('user').get("facility").then((f)=> return f.get("options"));
		});
	}
});
