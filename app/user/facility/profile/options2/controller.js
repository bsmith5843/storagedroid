import Ember from 'ember';

export default Ember.Controller.extend({
	fields: [
		["General Directions to the Facility","GeneralDirection"],
		["Neighborhoods","neighborhoods"],
		["About Facility Manager","AboutManager"],
		["About Facility Staff","AboutStaff"],
		["Other Sales (grills, ice, propane, etc.)","OtherSales"],
		["Associated Rental Trucks/Moving Company (UHaul, Budget, Ryder, Penske)","AssociatedRentals"]
		]
	
});
