import Ember from 'ember';

export default Ember.Controller.extend({
	inputs : [	
	["Enter First Name", "FirstName"],
	["Enter Last Name", "LastName"],
	["Enter Street Address", "Address"],
	['Country/State','Country/State', {component: 'country-region-select', id:"country", regionid :"region"}],	
	["Enter City", "City"],
	["Enter Postal Code", "Zip"],
	 ["Enter Phone #", "PhoneNo"]
	]
});
