/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    // Add options here
	 paths: [
      'app/styles'
    ]
  });

  var bowerdep = ['metro-dist/js/metro.min.js',
   'metro-dist/css/metro-icons.min.css',
  'image-scale/image-scale.js',
  'jquery-form/jquery.form.js',
  'bootstrap/js/tooltip.js',
  'bootstrap/js/popover.js',  
  'bootstrapx-clickover/js/bootstrapx-clickover.js',
  'country-region-selector/dist/jquery.crs.min.js',
];
  bowerdep.forEach(function(v) {
	 app.import("bower_components/" + v); 
 });
  var otherdep = [  'metro.min.css',
  "moment.js", 
  "jquery.filthypillow.js",
       "jquery.filthypillow.css",
	   "modernizr-custom.js",
	   "popoverhover.js"];
  otherdep.forEach(function(v) {
	 app.import("vendor/" + v); 
 });
 
  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  return app.toTree();
};
