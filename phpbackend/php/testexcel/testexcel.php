<?php

header("Access-Control-Allow-Origin: *");
require_once __DIR__ . "/../../vendor/autoload.php";
//set_time_limit(0);
date_default_timezone_set('Europe/London');
$inputFileName = $_FILES['uploadexcel']['tmp_name'];// __DIR__ . '/upload1.csv';

try {
    /** Load $inputFileName to a PHPExcel Object  **/
    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
} catch(PHPExcel_Reader_Exception $e) {
    die('Error loading file: '.$e->getMessage());
}

$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true);
$e = ['StorageFacilityName','AuctionDate','StartTime','EndTime','UnitNumber','UnitSize','AdditionalInfo','FacilityLogTagID'];
$v = [];
for($i=0; $i < count($sheetData); $i++)
{
	$a = valid($sheetData[$i]);
	
	if(!$a) {
		$err[]= $i;
		continue;
	}
	$v[] = array_combine($e,$a);
}
echo json_encode($v);
function valid($a)
{
	if(count($a) < 6) {
      return false;
	}
    for($i = 0; $i < 6; $i++)
	{
	   if(empty($a[$i])) {
		    return false;		   
	   }
	}
	for($i = 6; $i <8; $i++)
	{
	  $a[$i] = $a[$i] ?:  "";
	}
	return array_slice($a,0,8);
}

function row($a) {
	
	
	
}