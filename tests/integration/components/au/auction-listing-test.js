import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('au/auction-listing', 'Integration | Component | au/auction listing', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{au/auction-listing}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#au/auction-listing}}
      template block text
    {{/au/auction-listing}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
