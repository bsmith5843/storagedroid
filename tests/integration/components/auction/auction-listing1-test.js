import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('auction/auction-listing1', 'Integration | Component | auction/auction listing1', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{auction/auction-listing1}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#auction/auction-listing1}}
      template block text
    {{/auction/auction-listing1}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
