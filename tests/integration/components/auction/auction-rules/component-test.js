import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('auction/auction-rules', 'Integration | Component | auction/auction rules', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{auction/auction-rules}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#auction/auction-rules}}
      template block text
    {{/auction/auction-rules}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
