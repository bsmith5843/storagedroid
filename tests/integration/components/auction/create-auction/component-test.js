import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('auction/create-auction', 'Integration | Component | auction/create auction', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{auction/create-auction}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#auction/create-auction}}
      template block text
    {{/auction/create-auction}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
