import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('auction/list-auctions', 'Integration | Component | auction/list auctions', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{auction/list-auctions}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#auction/list-auctions}}
      template block text
    {{/auction/list-auctions}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
