import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('auctioneer-auction', 'Integration | Component | auctioneer auction', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{auctioneer-auction}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#auctioneer-auction}}
      template block text
    {{/auctioneer-auction}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
