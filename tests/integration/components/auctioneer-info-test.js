import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('auctioneer-info', 'Integration | Component | auctioneer info', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{auctioneer-info}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#auctioneer-info}}
      template block text
    {{/auctioneer-info}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
