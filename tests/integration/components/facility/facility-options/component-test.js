import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('facility/facility-options', 'Integration | Component | facility/facility options', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{facility/facility-options}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#facility/facility-options}}
      template block text
    {{/facility/facility-options}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
