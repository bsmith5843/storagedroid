import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('metro-ui-field', 'Integration | Component | metro ui field', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{metro-ui-field}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#metro-ui-field}}
      template block text
    {{/metro-ui-field}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
