import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('sf/auctioneer-search', 'Integration | Component | sf/auctioneer search', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{sf/auctioneer-search}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#sf/auctioneer-search}}
      template block text
    {{/sf/auctioneer-search}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
