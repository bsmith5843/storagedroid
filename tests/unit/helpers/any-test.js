import { any } from '../../../helpers/any';
import { module, test } from 'qunit';

module('Unit | Helper | any');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = any([42]);
  assert.ok(result);
});
