import { every } from '../../../helpers/every';
import { module, test } from 'qunit';

module('Unit | Helper | every');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = every([42]);
  assert.ok(result);
});
