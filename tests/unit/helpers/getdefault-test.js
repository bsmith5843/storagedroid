import { getdefault } from '../../../helpers/getdefault';
import { module, test } from 'qunit';

module('Unit | Helper | getdefault');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = getdefault([42]);
  assert.ok(result);
});
