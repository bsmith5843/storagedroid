/* jquery.filthypillow v.1.5.0
 * simple and fancy datetimepicker
 * by aef
 */
( function( factory ) {
	if ( typeof define === 'function' && define.amd ) {
		define( [ 'jquery' ], factory );
	} else if ( typeof exports === 'object' ) {
		module.exports = factory;
	} else {
		factory( jQuery );
	}
} ( function( $ ) {
  var pluginName = "timeinput",
      name = "plugin_" + pluginName,
      defaults = {
        startStep: "day",
        minDateTime: null,
        maxDateTime: null, //function returns moment obj
        initialDateTime: null, //function returns moment obj
        enableCalendar: false,
        steps: [ "month", "day", "hour", "minute", "meridiem" ],
				exitOnBackgroundClick: true,
        enable24HourTime: false,
        minuteStepSize: 15,
        calendar: {
					isPinned: false,
          saveOnDateSelect: false
        }
      },
      methods = [ "show", "hide", "destroy", "updateDateTime", "updateDateTimeUnit", "setTimeZone" ],
      returnableMethods = [ "getDate", "isValid" ];

  function FilthyPillow( $element, options ) {
		var calendarOptions = $.extend( {}, defaults.calendar, options.calendar || {} );


    if( options.enable24HourTime && !options.steps ) //remove meridiem
      options.steps = defaults.steps.slice( 0, -1 );

    this.options = $.extend( {}, defaults, options );

		this.options.calendar = calendarOptions;

    this.$element = $element;
    this.setup( );
  }
 /*       '<div class="fp-calendar">' +
                  '<span class="fp-month fp-option"></span>/<span class="fp-day fp-option"></span>' +
                '</div>' + */
  FilthyPillow.prototype = {
    template: '<div class="fp-container">' +
         
                '<div class="fp-clock">' +
                  '<span class="fp-hour fp-option"></span>:<span class="fp-minute fp-option"></span>' +
                  '<span class="fp-meridiem fp-option"></span>' +
                '</div>' +
				
             /*    '<div class="fp-save"><button class="btn btn-primary fp-save-button" type="button">Save</button></div>' +
                '<div class="fp-description"></div>' +
                '<div class="fp-errors"></div>' + */
               
              '</div>',
    currentStep: null,
    dateTime: null,
    currentTimeZone: null, //null is browser default
    currentDigit: 0, //first or second digit for key press
    isActiveLeadingZero: 0, //user types in 0 as first digit
    stepRegExp: null,
    isError: false, //error is being shown
    isActive: false, //whether the calendar is active or not

    setup: function( ) {
      this.steps = this.options.steps;
      this.stepRegExp = new RegExp( this.steps.join( "|" ) )
      this.$window = $( window );
      this.$document = $( document );
      this.$body = $( "body" );
      this.id = "filthypillow-" + Math.round( Math.random( ) * 1000 );
      this.init = this.options.init;
      this.$container = $( this.template );
      this.$container.attr( "id", this.id );

      this.$calendar = this.$container.find( ".fp-calendar" );
      this.$options = this.$container.find( ".fp-option" );

      this.$month = this.$calendar.find( ".fp-month" );
      this.$day = this.$calendar.find( ".fp-day" );

      this.$clock = this.$container.find( ".fp-clock" );
      this.$hour = this.$clock.find( ".fp-hour" );
      this.$minute = this.$clock.find( ".fp-minute" );
      this.$meridiem = this.$clock.find( ".fp-meridiem" );

      this.$errorBox = this.$container.find( ".fp-errors" );
      this.$saveButton = this.$container.find( ".fp-save-button" );
      this.$descriptionBox = this.$container.find( ".fp-description" );


      if( this.options.enable24HourTime )
        this.$meridiem.hide( );
      if( this.options.enableCalendar )
        

      this.setInitialDateTime( );
    },
    showError: function( step, errorMessage ) {
      this[ "$" + step ].addClass( "fp-error fp-out-of-range" );
      this.$errorBox.text( errorMessage ).show( );
      this.$saveButton.attr( "disabled", "disabled" );
      this.isError = true;
    },
    hideError: function( step, errorMessage ) {
      this.$container.find( ".fp-error" ).removeClass( "fp-error" );
      this.$errorBox.hide( );
      this.$saveButton.removeAttr( "disabled", "disabled" );
      this.isError = false;
    },
    activateSelectorTool: function( step ) {
      var $element = this[ "$" + step ];
      this.currentStep = step;

      //Highlight element
      this.$container.find( ".active" ).removeClass( "active" );
      $element.addClass( "active" );

      //Reset digit
      this.currentDigit = 0;
      this.isActiveLeadingZero = 0;
     
    },

    to12Hour: function( value ) {
      if( this.dateTime.format( "A" ) === "PM" && value > 12 )
        return value - 12;
      return value;
    },

    to24Hour: function( value ) {
      if( this.dateTime.format( "A" ) === "PM" && value < 12 )
        return value + 12;
      else if( this.dateTime.format( "A" ) === "AM" && value > 11 )
        return value - 12;
      return value;
    },

    formatToMoment: function( step, value ) {
      if( step === "month" )
        return value - 1;
      return value;
    },

    formatFromMoment: function( step, value ) {
      if( step === "month" )
        return value + 1;
      return value;
    },

    isValidDigitInput: function( digits ) {
      digits = parseInt( digits, 10 );
      if( this.currentStep === "month" ) {
        if( digits > 12 )
          return false;
      }
      else if( this.currentStep === "day" ) {
        if( digits > this.dateTime.daysInMonth( ) )
          return false;
      }

      return true;
    },

    updateDigit: function( step, digit, value ) {
      var fakeValue, precedingDigit, moveNext = false;

      if( step === "meridiem" )
        return;

      if( step === "day" )
        step = "date"; //see moment getter/setter docs

      if( digit === 1 && value === 0 ) {
        this.isActiveLeadingZero = 1;
        return;
      }

      if( digit === 2 && !this.isActiveLeadingZero ) {
        precedingDigit = this.dateTime.get( step );
        if( step === "hour" )
          precedingDigit = this.to12Hour( precedingDigit )
        else
          precedingDigit = this.formatFromMoment( step, precedingDigit );
        fakeValue = parseInt( precedingDigit + "" + value, 10 );
      }
      else
        this.isActiveLeadingZero = 0;

      fakeValue = fakeValue || value;

      if( step === "hour" ) { //this is retain the current meridiem
        if( !this.options.enable24HourTime )
          fakeValue = this.to24Hour( fakeValue );
      }
      else
        fakeValue = this.formatToMoment( step, fakeValue );

			if( !this.isValidDigitInput( fakeValue ) ) {
        if( this.currentDigit === 2 )
          this.currentDigit = 1;
        return;
      }

      if( this.currentDigit === 2 )
        moveNext = true;
      else if( step === "month" && value > 1 )
        moveNext = true;
      else if( step === "date" && value > 3 )
        moveNext = true;
      else if( step === "hour" && ( ( value > 1 && !this.options.enable24HourTime ) || value > 2 ) )
        moveNext = true;
      else if( step === "minute" && value > 5 )
        moveNext = true;

      this.updateDateTimeUnit( step, fakeValue, moveNext );
    },

    onOptionClick: function( e ) {
      var $target = $( e.target ),
          classes = $target.attr( "class" ),
      //figure out which step was clicked
          step = classes.match( this.stepRegExp );
      if( step && step.length )
        this.activateSelectorTool( step[ 0 ] );
    },

    onKeyUp: function( e ) {
		if(!this.$container.find( ".active" ).length) {return;}
      var keyCode = e.keyCode || e.which;
      if( this.currentStep === "meridiem" )
	  { this.$element.trigger("keyup1");
	  return;}

      if( keyCode === 8 ) //backspace
        this.currentDigit -= 1;

      //0-9 with numpad support
      if( ( keyCode >= 48 && keyCode <= 57 ) || ( keyCode >= 96 && keyCode <= 105 ) ) {
        this.currentDigit += 1;
        this.updateDigit( this.currentStep, this.currentDigit, keyCode % 48 );
      }

      if( this.currentDigit === 2 )
        this.currentDigit = 0;
	  this.$element.trigger("keyup1");
    },

    onKeyDown: function( e ) {
		if(!this.$container.find( ".active" ).length) {return;}
      var keyCode = e.keyCode || e.which;
      switch( keyCode ) {
        case 38: this.moveUp( ); break; //up
        case 40: this.moveDown( ); break; //down
        case 37: this.moveLeft( ); break; //left
        case 39: this.moveRight( ); break; //right
      }
      if( e.shiftKey && keyCode === 9 ) //shift + tab
        this.moveLeft( );
      else if( keyCode === 9 ) //tab
        this.moveRight( );

      if( keyCode === 13 ) //enter - lets them save on enter
        this.$saveButton.click( );
       
      //prevents page from moving left/right/up/down/submitting form on enter
      return false;
    },

    moveDown: function( ) {
      if( this.currentStep === "meridiem" ) {
        //We do this so the day does not change
        var offset = this.dateTime.format( "H" ) < 12 ? 12 : -12;
        this.changeDateTimeUnit( "hour", offset );
      }
      else if( this.currentStep === "minute" )
        this.changeDateTimeUnit( this.currentStep, parseInt(this.options.minuteStepSize) * -1, 10 );
      else if( this.currentStep )
        this.changeDateTimeUnit( this.currentStep, -1 );

      this.currentDigit = 0;
    },

    moveUp: function( ) {
      if( this.currentStep === "meridiem" ) {
        var offset = parseInt( this.dateTime.format( "H" ), 10 ) < 12 ? 12 : -12;

        this.changeDateTimeUnit( "hour", offset );
      }
      else if( this.currentStep === "minute" )
        this.changeDateTimeUnit( this.currentStep, parseInt(this.options.minuteStepSize, 10) );
      else if( this.currentStep )
        this.changeDateTimeUnit( this.currentStep, 1 );

      this.currentDigit = 0;
    },

    moveLeft: function( ) {
		
      if( !this.currentStep) return;
	  if(this.currentStep == "hour") {
		 if(this.options.end) {
		  this.$element.find(".fp-option").eq(2).click();
		 }
		 return;
	  }
      var i = this.steps.indexOf( this.currentStep );
      if( i === 0 ) i = this.steps.length - 1;
      else i -= 1;
      this.activateSelectorTool( this.steps[ i ] );
	 
    },

    moveRight: function( ) {
	
      if( !this.currentStep ) return;
	  if( this.currentStep == "meridiem") {
		  if(this.options.start) {
			  var t = this;
			  setTimeout(function(){  t.$element.find(".fp-option").eq(3).click();},10);
		  }
		 return;
	  }
      var i = this.steps.indexOf( this.currentStep );
      if( i === this.steps.length - 1 ) i = 0;
      else i += 1;
      this.activateSelectorTool( this.steps[ i ] );
	  
    },

    onClickToExit: function( e ) {
      var $target = $( e.target );
      if(
          //TODO: testing for class is shit but closest doesn't work on td day select
          //for some reason
         // !/fp-/.test( $target.attr( "class" ) ) &&
          !$target.closest( this.$container ).length 
		  && (!$target.closest( this.$element ).length || $target.closest(".fp-container").length )
		 ) 
		  {
        //this.hide( );
		
		this.$container.find( ".active" ).removeClass( "active" );
		this.isActive = false;
      } else if($target.closest( this.$element ).length && !this.$element.find( ".active" ).length) {
		  /*var t  = this;
		  setTimeout(function() {
		  t.$element.find(".fp-container:first").click();},100);//get(0).click();//click();*/
		  e.target = this.$element.find(".fp-option:first");
		  this.onOptionClick(e);
	  }
    },

    onSave: function( ) {
      if( this.isInRange( this.dateTime ) )
        this.$element.trigger( "fp:save", [ this.dateTime ] );
    },

    addEvents: function( ) {
      this.$options.on( "click", $.proxy( this.onOptionClick, this ) );
      this.$saveButton.on( "click", $.proxy( this.onSave, this ) );

      this.$document.on( "keydown." + this.id, $.proxy( this.onKeyDown, this ) );
      this.$document.on( "keyup." + this.id, $.proxy( this.onKeyUp, this ) );
			if( this.options.exitOnBackgroundClick )
				this.$window.on( "click." + this.id, $.proxy( this.onClickToExit, this ) );
    },

    removeEvents: function( ) {

      this.$options.off( "click" );
      this.$saveButton.off( "click" );
      this.$window.off( "click." + this.id );

      this.$document.off( "keydown." + this.id );
      this.$document.off( "keyup." + this.id );
    },
    setDateTime: function( dateObj, moveNext ) {
      this.dateTime = moment( dateObj );

      if( this.options.enableCalendar )
       // this.calendar.setDate( this.dateTime );

      if( !this.isInRange( this.dateTime ) )
        this.showError( this.currentStep, "Date is out of range, please fix." );
      else if( this.isError )
        this.hideError( );

      if( !this.isError && moveNext )
        this.moveRight( );
    },
    renderDateTime: function( ) {
      this.$month.text( this.dateTime.format( "MM" ) );
      this.$day.text( this.dateTime.format( "DD" ) );
      this.$hour.text( this.dateTime.format( !this.options.enable24HourTime ? "hh" : "HH" )  );
      this.$minute.text( this.dateTime.format( "mm" ) );
      if( !this.options.enable24HourTime )
        this.$meridiem.text( this.dateTime.format( "A" ) );

      this.$descriptionBox.text( this.dateTime.format( "LLLL" ) );
    },
    setInitialDateTime: function( ) {
      var m = moment( this.init || "0000-00-00 00:00:00"),
          minutes = m.format( "m" );
      m.zone( this.currentTimeZone );

      //Initial value are done in increments of 15 from now.
      //If the time between now and 15 minutes from now is less than 5 minutes,
      //go onto the next 15.
      if( typeof this.options.initialDateTime === "function" )
        m = this.options.initialDateTime( m.clone( ) );
	
     this.setDateTime( m);
	 if(!this.init) {
	 this.$hour.text("--"  );
      this.$minute.text("--");
	  this.$meridiem.text("--");
	 } else {
		this.renderDateTime();
	 }
    },

    isInRange: function( date ) {
      var minDateTime = typeof this.options.minDateTime === "function" && this.options.minDateTime( ),
          maxDateTime = typeof this.options.maxDateTime === "function" && this.options.maxDateTime( ),
          left = right = false;

      if( minDateTime ) {
        minDateTime.zone( this.currentTimeZone );
        left = date.diff( minDateTime, "second" ) < 0;
      }
      if( maxDateTime ) {
        maxDateTime.zone( this.currentTimeZone );
        right = date.diff( maxDateTime, "second" ) > 0;
      }

      return !( right || left )
    },

    setTimeZone: function( zone ) {
      this.dateTime.zone( zone );
      this.currentTimeZone = zone;

   
    },

    dateChange: function( ) {
     
      this.$element.trigger("fp:datetimechange", [ this.dateTime ]);
    },

    changeDateTimeUnit: function( unit, value ) {
      var tmpDateTime = this.dateTime.clone( ).add( value, unit + "s" ),
          isInRange = this.isInRange( tmpDateTime );

      if( !this.isError && !isInRange )
        this.showError( this.currentStep, "Date is out of range, please fix." );
      else if( isInRange )
        this.hideError( );

      this.dateTime.add( value, unit + "s" );
      this.renderDateTime( );

      this.dateChange( );
    },
    //api
    updateDateTimeUnit: function( unit, value, moveNext ) {
      var dateObj = this.dateTime.clone( ).set( unit, value );
      this.updateDateTime( dateObj, moveNext );
    },
    getDate: function( ) {
      return this.dateTime.clone( );
    },
		isValid: function( ) {
			return !this.isError;
		},
    updateDateTime: function( dateObj, moveNext ) {
      this.setDateTime( dateObj, moveNext );
      this.renderDateTime( );
      this.dateChange( );
    },
    show: function( ) {
      if( !this.isActive ) {
        this.setInitialDateTime( );
		this.$element.append(this.$container);//insertAfter( this.$element );
        //this.$container.insertAfter( this.$element );
        this.activateSelectorTool( this.options.startStep );
        this.addEvents( );
        this.isActive = true;
      }
    },
    hide: function( ) {
      if( this.isActive ) {
        this.$container.remove( );
        this.removeEvents( );
        this.isActive = false;
      }
    },
    destroy: function( ) {
      this.removeEvents( );
      this.$container.remove( );
      this.isActive = false;
      this.$element.removeData( name );
    }
  };


  $.fn[ pluginName ] = function( optionsOrMethod ) {
    var $this,
        _arguments = Array.prototype.slice.call( arguments ),
        optionsOrMethod = optionsOrMethod || { };
    //Initialize a new version of the plugin
    if( ( typeof optionsOrMethod ).toLowerCase( ) === "string" && ~$.inArray( optionsOrMethod, returnableMethods ) )
      return this.data( name )[ optionsOrMethod ].apply( this.data( name ), _arguments.slice( 1, _arguments.length ) );
    else {
      return this.each(function ( ) {
        $this = $( this );
        if( ( typeof optionsOrMethod ).toLowerCase( ) === "object" )
          $this.data( name, new FilthyPillow( $this, optionsOrMethod ) );
        else if( ( typeof optionsOrMethod ).toLowerCase( ) === "string" ) {
          if( ~$.inArray( optionsOrMethod, methods ) )
            $this.data( name )[ optionsOrMethod ].apply( $this.data( name ), _arguments.slice( 1, _arguments.length ) );
          else
            throw new Error( "Method " + optionsOrMethod + " does not exist. Did you instantiate filthypillow?" );
        }
      } );
    }
  };
} ) );
